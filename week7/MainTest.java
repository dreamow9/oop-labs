/**
* DrawProgram
*@author Pham Duc Duy
*@since 09/10/2018
*@version 1.0
*/
package DrawProgram;
import java.util.*;

public class MainTest{
  public static void main(String[] args) {
    Layer l1 = new Layer();
    Triangle t1 = new Triangle(1, 2, 3, "red");
    Triangle t2 = new Triangle(2, 2, 2, "blue");
    Circle c1 = new Circle(3, 3, 5, "black");
    Hexagon h1 = new Hexagon(1, 1, 1, "white");
    Hexagon h2 = new Hexagon(2, 2, 2, "brown");
    Hexagon h3 = new Hexagon(3, 3, 3, "silver");
    Hexagon h4 = new Hexagon(3, 3, 3, "silver");
    l1.addShape(t1);
    l1.addShape(t2);
    l1.addShape(h3);
    l1.addShape(c1);
    l1.addShape(h1);
    l1.addShape(h4);
    l1.addShape(h2);
    /**
    * test deleteCircle
    */
    //l1.printLayer();
    //l1.deleteCircle();
    //l1.printLayer();
    Diagram d1 = new Diagram();
    d1.addLayer(l1);
    /**
    * test removeCircle
    */
    //d1.printDiagram();
    //d1.removeCircle();
    //d1.printDiagram();
    /**
    * test xoa cac shape trung nhau
    */
    //l1.deleteSameShapes();
    //l1.printLayer();
    /**
    * test dua cac hinh vao cung 1 layer
    */
    d1.arrangeShapes();
    d1.printArrangedShapes();
  }
}
