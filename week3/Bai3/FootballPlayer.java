package Bai3;

/**
 * class footvall player
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class FootballPlayer {
    private String name;
    private String club;
    private String playingPositon;

    /**
     * getter
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter
     * @return club
     */
    public String getClub() {
        return club;
    }

    /**
     * setter
     * @param club club
     */
    public void setClub(String club) {
        this.club = club;
    }

    /**
     * getter
     * @return playing position
     */
    public String getPlayingPositon() {
        return playingPositon;
    }

    /**
     * setter
     * @param playingPositon playing position
     */
    public void setPlayingPositon(String playingPositon) {
        this.playingPositon = playingPositon;
    }
}
