/**
 * Hello world
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class HelloWorld {
    /**
     * class main
     * @param args khong su dung
     */
    public static void main(String[] args){
        System.out.println("Hello World, Java!");
    }
}
