package Bai3;

/**
 * class lamp
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Lamp {
    private String color;
    private int price;
    private int weight;

    /**
     * getter
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * setter
     * @param color color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * getter
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * setter price
     * @param price price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * getter
     * @return prioe
     */
    public int getWeight() {
        return weight;
    }

    /**
     * setter
     * @param weight weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }
}
