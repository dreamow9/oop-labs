/**
* HoaQua la class chi cac loai hoa qua noi chung gom nhung gia tri ten, ma hang, ngay nhap, mau sac, gia giaBan
*/
class HoaQua{
  private String name;
  private String maHang;
  private String ngayNhap;
  private String mauSac;
  private String giaBan;
  /**
  * lay gia tri ve ten cua loai qua
  *@param khong co bien dau vao
  *@return gia tri ten cua loai qua
  */
  public String getName(){
    return this.name;
  }
  /**
  * dat ten cho loai qua
  *@param s ten loai qua duoc dua vao
  *@return khong tra lai gia tri nao
  */
  public void setName(String s){
    this.name = s;
  }
  /**
  * lay gia tri ve ma hang cua loai qua
  *@param khong co bien dau vao
  *@return gia tri ma hang cua loai qua
  */
  public String getMaHang(){
    return this.maHang;
  }
  /**
  * dat ma hang cho loai qua
  *@param s ma hang cua loai qua duoc dua vao
  *@return khong tra lai gia tri nao
  */
  public void setMaHang(String s){
    this.maHang = s;
  }
  /**
  * lay gia tri ve ngay nhap ve cua loai qua
  *@param khong co bien dau vao
  *@return gia tri ngay nhap ve cua loai qua
  */
  public String getngayNhap(){
    return this.ngayNhap;
  }
  /**
  * dat ngay nhap cho loai qua
  *@param s ngay nhap cua loai qua duoc dua vao
  *@return khong tra lai gia tri nao
  */
  public void setNgayNhap(String s){
    this.ngayNhap = s;
  }
  /**
  * lay gia tri ve mau sac cua loai qua
  *@param khong co bien dau vao
  *@return gia tri mau sac cua loai qua
  */
  public String getMauSac(){
    return this.mauSac;
  }
  /**
  * dat mau sac cho loai qua
  *@param s mau sac cua loai qua duoc dua vao
  *@return khong tra lai gia tri nao
  */
  public void SetMauSac(String s){
    this.mauSac = s;
  }
  /**
  * lay gia tri ve gia ban cua loai qua
  *@param khong co bien dau vao
  *@return gia tri gia ban cua loai qua
  */
  public String getGiaBan(){
    return this.giaBan;
  }
  /**
  * dat gia bam cho loai qua
  *@param s gia ban cua loai qua duoc dua vao
  *@return khong tra lai gia tri nao
  */
  public void setGiaBan(String s){
    this.giaBan = s;
  }
  /**
  * lay mot so thong tin co ban cua loai qua
  *@param khong co bien dau vao
  *@return 1 sau the hien thong tin cua loai qua
  */
  public void getInfo(){
    System.out.println(this.getName() + "   | Ma hang: " + this.getMaHang() + " |  Gia: "  + this.getGiaBan() + " | mau sac: " + this.getMauSac());
  }
}
/**
* QuaCam co quan he is-a voi HoaQua
*/
class QuaCam extends HoaQua{
  private String soMui;
  /**
  * khoi tao cac gia tri cho qua cam
  */
  public QuaCam(){
    this.SetMauSac("cam");
  }
  /**
  * lay gia tri ve so mui cua qua cam
  *@param khong co bien dau vao
  *@return gia tri so mui cua qua cam
  */
  public String getSoMui(){
    return this.soMui;
  }
  /**
  * dat gia tri ve so mui cua qua cam
  *@param s la so mui cua qua cam
  *@return khong tra lai gia tri nao
  */
  public void setSoMui(String s){
    this.soMui = s;
  }
}
/**
* QuaTao co quan he is-a voi HoaQua
*/
class QuaTao extends HoaQua{
  private String canNang;
  /**
  *khoi tao cac gia tri ban dau cho qua tao
  */
  public QuaTao(){
    this.SetMauSac("do");
    this.setName("Tao");
    this.setMaHang("T");
    this.setGiaBan("30 000 VND");
    this.setCanNang("3g");
  }
  /**
  * lay gia tri ve can nang cua qua Tao
  *@param khong co bien dau vao
  *@return gia tri can nang cua qua tao
  */
  public String getCanNang(){
    return this.canNang;
  }
  /**
  * dat gia tri can nang cho qua Tao
  *@param s can nang dau vao
  *@return khong tra lai gia tri nao
  */
  public void setCanNang(String s){
    this.canNang = s;
  }
}
/**
* CamCaoPhong co quan he is-a voi QuaCam
*/
class CamCaoPhong extends QuaCam{
  /**
  *khoi tao cac gia tri cho cam cao phong
  */
  public CamCaoPhong(){
    this.setName("Cam Cao Phong");
    this.setMaHang("CCP");
    this.setGiaBan("50 000 VND");
    this.setSoMui("nhieu mui");
  }
}
/**
* CamSanh co quan he is-a voi QuaCam
*/
class CamSanh extends QuaCam{
  /**
  *khoi tao cac gia tri cho cam sanh
  */
  public CamSanh(){
    this.setName("Cam sanh");
    this.setMaHang("CS");
    this.setGiaBan("45 000 VND");
    this.setSoMui("it mui");
  }
}
/**
* class Fruits dung de khoi tao cac doi tuong cam cao phong, cam sanh, Tao
*@author Pham Duc Duy
*@version 1.0
*@since 1/10/2018
*/
public class Fruits {
  /**
  *ham main thuc hien cac chuc nang chinh
  *@param args khong su dung
  *@return khong tra lai gia tri gi
  */
  public static void main(String[] args) {
    CamCaoPhong f1 = new CamCaoPhong();
    CamSanh f2 = new CamSanh();
    QuaTao f3 = new QuaTao();
    f1.getInfo();
    f2.getInfo();
    f3.getInfo();
  }
}
