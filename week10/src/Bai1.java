import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Bai1 {
    /**
     * lay tat ca cac static function
     * @param path
     * @return danh sach cac function
     * @throws IOException
     */
    public static List<String> getAllFunctions(File path) throws IOException {
        List<String> ans = new ArrayList<String>();
        if (path.isFile()){
            Scanner sc = new Scanner(path);
            while (sc.hasNext()){
                String s = sc.nextLine();
                if (s.contains("static")){
                    if (s.contains("/*") || s.contains("*\\"))
                        continue;
                    if (s.indexOf("//") == 0) continue;
                    int cmt = s.indexOf("//");
                    int endOfName = s.lastIndexOf(")");
                    if (cmt >= 0 && cmt < endOfName) continue;
                    int n = 0;
                    while (sc.hasNext()){
                        String st = sc.nextLine();
                        if (st.contains("/*") || st.contains("*\\"))
                            continue;
                        if (st.indexOf("//") == 0) continue;
                        if (st.indexOf("//") > 0 && st.indexOf("//") < st.lastIndexOf(";")) continue;
                        if (st.contains("{")) n++;
                        if (st.contains("}")) {
                            n--;
                            if (n == 0) {
                                s = s + "\n" + st;
                                break;
                            }
                        }
                        s = s + "\n" + st;
                    }
                    ans.add(s);
                }
            }
        }
        return ans;
    }

    /**
     * tim function theo ten
     * @param name
     * @return ten cua function tim dc
     */
    public static String findFunctionByName(String name){
        String ans = "";
        String functionName = name.substring(0, name.lastIndexOf('('));
        List<String> paras = new ArrayList<>();
        int n = name.indexOf(',', 0);
        int last = name.lastIndexOf('(');
        if (n == -1) paras.add(name.substring(last + 1, name.lastIndexOf(')')));
        while (n != -1){
            paras.add(name.substring(last, n));
            last = n + 1;
            n = name.indexOf(',', last);
        }
        File path = new File ("./src/Utils.java");
        try {
            Scanner sc = new Scanner(path);
            while (sc.hasNext()){
                String s = sc.nextLine();
                if (s.contains("/*") || s.contains("*\\"))
                    continue;
                if (s.indexOf("//") == 0) continue;
                if (s.indexOf("//") > 0 && s.indexOf("//") < s.lastIndexOf(")")) continue;
                if (s.contains(functionName)){
                    boolean c = true;
                    List<String> sparas = new ArrayList<>();
                    int n1 = s.indexOf(',', 0);
                    if (n1 == -1) {
                        if (s.lastIndexOf(' ') < s.length() && s.lastIndexOf(' ') > 0)
                            sparas.add(s.substring(s.lastIndexOf('(') + 1, s.indexOf(' ', s.lastIndexOf('('))));
                    }
                    int last1 = s.lastIndexOf('(');
                    while (n1 != -1){
                        sparas.add(s.substring(last1, n1));
                        last1 = n1 + 1;
                        n1 = s.indexOf(',', last1);
                    }

                    if (paras.size() != sparas.size()) {
                        c = false;
                    }
                    else{
                        for (int i = 0; i < paras.size(); i++){
                            if (!paras.get(i).equals(sparas.get(i)))
                                c = false;
                        }
                    }
                    if (c) ans = s;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return ans;
    }

    /**
     * ham main
     * @param args
     */
    public static void main(String[] args){
        File path = new File ("./src/Utils.java");
        try {
            List<String> allFunc = getAllFunctions(path);
            for (String i : allFunc){
                System.out.println(i);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(findFunctionByName("readContentFromFile(String)"));
    }
}
