/**
* BiggerNumber helps us find bigger number between two numbers
*@author Pham Duc Duy
*@version 1.0
*@since 25/09/2018
*/
public class BiggerNumber{
	/**
	* findBiggerNumber find the bigger number btw two numbers
	*@param a the first number
	*@param b the second number
	*@return the bigger number
	*/
	public static int findBiggerNumber(int a, int b){
		if (a > b) return a;
		return b;
	}
}