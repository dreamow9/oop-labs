package Bai2;

/**
 * class phan so
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class PhanSo {
    private int tuSo;
    private int mauSo;

    /**
     * constructor
     * @param tuSo tu so
     * @param mauSo mau so
     */
    public PhanSo(int tuSo, int mauSo){
        if (mauSo == 0){
            System.out.println("error");
        }
        else {
            this.tuSo = tuSo;
            this.mauSo = mauSo;
        }
    }

    /**
     * getter
     * @return tu so
     */
    public int getTuSo() {
        return tuSo;
    }

    /**
     * setter
     * @param tuSo tu so
     */
    public void setTuSo(int tuSo) {
        this.tuSo = tuSo;
    }

    /**
     * getter
     * @return mau so
     */
    public int getMauSo() {
        return mauSo;
    }

    /**
     * setter
     * @param mauSo mau so
     */
    public void setMauSo(int mauSo) {
        this.mauSo = mauSo;
    }

    /**
     * tim ucln
     * @param a bien thu nhat
     * @param b bien thu hai
     * @return ucln cua a va b
     */
    public int findGCD(int a, int b){
        if (a < b){
            int c = b;
            b = a;
            a = c;
        }
        int tmp;
        while (b != 0){
            tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    /**
     * toi gian hoa phan so
     */
    public void toiGian(){
        int ucln = findGCD(tuSo, mauSo);
        this.tuSo /= ucln;
        this.mauSo /= ucln;
    }

    /**
     * cong phan so voi 1 phan so khac
     * @param ps phan so cong vao
     */
    public void congPS(PhanSo ps){
        int ts = this.tuSo * ps.getMauSo() + ps.getTuSo() * this.mauSo;
        int ms = this.mauSo * ps.getMauSo();
        this.tuSo = ts;
        this.mauSo = ms;
        this.toiGian();
    }

    /**
     * tru phan so voi 1 phan so khac
     * @param ps phan so can tru
     */
    public void truPS(PhanSo ps){
        int ts = this.tuSo * ps.getMauSo() - ps.getTuSo() * this.mauSo;
        int ms = this.mauSo * ps.getMauSo();
        this.tuSo = ts;
        this.mauSo = ms;
        this.toiGian();
    }

    /**
     * nhan phan so voi 1 phan so khac
     * @param ps  phan so can nhan
     */
    public void nhanPS(PhanSo ps){
        int ts = this.tuSo * ps.getTuSo();
        int ms = this.mauSo * ps.getMauSo();
        this.tuSo = ts;
        this.mauSo = ms;
        this.toiGian();
    }

    /**
     * chia phan so voi 1 phan so khac
     * @param ps phan so can chia
     */
    public void chiaPS(PhanSo ps){
        int ts = this.tuSo * ps.getMauSo();
        int ms = this.mauSo * ps.getTuSo();
        this.tuSo = ts;
        this.mauSo = ms;
        this.toiGian();
    }

    /**
     * in phan so
     */
    public void printPS(){
        if (mauSo == 1) System.out.println(tuSo);
        else if (mauSo == -1) System.out.println(-tuSo);
        else System.out.println(this.tuSo + "/" + this.mauSo);
    }

}
