package Bai3;

/**
 * class shirt
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Shirt {
    private String color;
    private int price;
    private String material;

    /**
     * getter
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * setter
     * @param color color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * getter
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * setter
     * @param price price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * getter
     * @return material
     */
    public String getMaterial() {
        return material;
    }

    /**
     * setter
     * @param material material
     */
    public void setMaterial(String material) {
        this.material = material;
    }
}
