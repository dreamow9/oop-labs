import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class BMITests {
	@Test
	public void test() {
		BMI test = new BMI();
		String res = test.BMICal(55, 1.7);
		assertEquals(res, "Binh thuong");

		res = test.BMICal(70, 1.7);
		assertEquals(res, "Thua can");

		res = test.BMICal(90, 1.7);
		assertEquals(res, "Beo phi");

		res = test.BMICal(40, 1.7);
		assertEquals(res, "Thieu can");

		res = test.BMICal(55, 1.7);
		assertEquals(res, "Binh thuong");
	}

}