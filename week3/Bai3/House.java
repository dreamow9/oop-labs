package Bai3;

/**
 * class house
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class House {
    private int numberOfFloors;
    private String color;

    /**
     * getter
     * @return number of floors
     */
    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    /**
     * setter
     * @param numberOfFloors number of floors
     */
    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    /**
     * getter
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * setter
     * @param color color
     */
    public void setColor(String color) {
        this.color = color;
    }
}
