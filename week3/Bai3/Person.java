package Bai3;

/**
 * class person
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Person {
    private int age;
    private String sex;
    private String job;

    /**
     * getter
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * setter
     * @param age age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * getter
     * @return sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * setter
     * @param sex sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * getter
     * @return job
     */
    public String getJob() {
        return job;
    }

    /**
     * setter
     * @param job job
     */
    public void setJob(String job) {
        this.job = job;
    }
}
