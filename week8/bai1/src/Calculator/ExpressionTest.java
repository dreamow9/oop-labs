/**
 * chuong trinh tinh toan
 * @author Pham Duc Duy
 * @since 23/10/2018
 * @version 1.0
 */
package Calculator;
public class ExpressionTest {
    /**
     * ham main
     * @param args
     */
    public static void main(String[] args){
        Numeral a = new Numeral(10);
        Square aSquare = new Square(a);
        Numeral b = new Numeral(1);
        Subtraction sub = new Subtraction(aSquare, b);
        Numeral c = new Numeral(2);
        Numeral d = new Numeral(3);
        Multiplication mul = new Multiplication(c, d);
        Addition add = new Addition(sub, mul);
        Square ans = new Square(add);
        System.out.println(ans.toString());
        /**
         * bat loi chia cho 0
         */
        try{
            Numeral x = new Numeral(10);
            Numeral y = new Numeral(0);
            Division div = new Division(x, y);
            System.out.println(div.evaluate());
        }
        catch (ArithmeticException e){
            System.out.println("Loi chia cho 0");
        }
    }
}
