package Bai3;

/**
 * class earphone
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Earphone {
    private int price;
    private String Company;

    /**
     * getter
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * setter
     * @param price price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * getter
     * @return company
     */
    public String getCompany() {
        return Company;
    }

    /**
     * setter
     * @param company company
     */
    public void setCompany(String company) {
        Company = company;
    }
}
