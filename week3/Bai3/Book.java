package Bai3;

/**
 * class Book
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Book {
    private int price;
    private int pages;
    private String category;
    private String author;

    /**
     * getter
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * setter
     * @param price price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * getter
     * @return pages
     */
    public int getPages() {
        return pages;
    }

    /**
     * setter
     * @param pages pages
     */
    public void setPages(int pages) {
        this.pages = pages;
    }

    /**
     * getter
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     * setter
     * @param category category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * getter
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * setter
     * @param author author
     */
    public void setAuthor(String author) {
        this.author = author;
    }
}
