import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


public class JSmallest {
	@Test
	public void test() {
	Smallest test = new Smallest();
	int a[]= {1,2,3,4,5};
	int n=5;
	int res = test.findSmallestNumber(a, n);
	assertEquals(res,1);
	
	int b[]= {-5,2,3,10,-10};
	res = test.findSmallestNumber(b, n);
	assertEquals(res, -10);
	
	int c[]= {100,2,3,-99,-10};
	res = test.findSmallestNumber(c, n);
	assertEquals(res, -99);
	
	int d[]= {100,200,300,10,-10};
	res = test.findSmallestNumber(d, n);
	assertEquals(res, -10);
	
	int e[]= {0,0,3,10,-5};
	res = test.findSmallestNumber(e, n);
	assertEquals(res, -5);
	
	}
}