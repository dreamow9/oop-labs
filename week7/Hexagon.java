package DrawProgram;


/**
* Hexagon co quan he is-a voi shape
*/

public class Hexagon extends Shape
{
	public Hexagon (double x, double y, double size, String color){
		super(x, y, size, color);
	}
	public void getInfo(){
		System.out.print("Hexagon: ");
		super.getInfo();
	}
}
