package DrawProgram;
import java.util.*;
/**
* class Layer chua cac shapes
*/

public class Layer extends Diagram {
	private ArrayList<Shape> shapes;
	/**
	* constructor
	*/
	public Layer(){
		shapes = new ArrayList<Shape>();
	}
	/**
	*them shape
	*/
	public void addShape(Shape s){
		shapes.add(s);
	}
	/**
	* xoa tat cac cac Triangle
	*/
	public void deleteTriangle(){
		for (Shape s : shapes){
				if (s instanceof Triangle)
					shapes.remove(s);
		}
	}
	/**
	* xoa tat cac cac Circle trong Layer
	*/
	public void deleteCircle(){
		for (Shape s : shapes){
			if (s instanceof Circle)
				shapes.remove(s);
		}
	}
	/**
	in ra cac hinh trong layer
	*/
	public void printLayer(){
		for (int i = 0; i < shapes.size(); i++){
			shapes.get(i).getInfo();
		}
	}
}
