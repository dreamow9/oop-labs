package Bai3;

/**
 * class website
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Website {
    public String url;
    public String description;

    /**
     * getter
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * setter
     * @param url url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * getter
     * @return description
     */
    public String getDecreption() {
        return description;
    }

    /**
     * setter
     * @param decreption desciption
     */
    public void setDecreption(String decreption) {
        this.description = decreption;
    }
}
