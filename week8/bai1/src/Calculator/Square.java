package Calculator;

import Calculator.Expression;

public class Square extends Expression {
    private Expression expression;
    public Square(Expression e) {
        this.expression = e;
    }

    @Override
    public String toString() {
        int result = expression.evaluate() * expression.evaluate();
        return result + "";
    }

    @Override
    public int evaluate() {
        return expression.evaluate() * expression.evaluate();
    }
}
