public class Bai2 {
    /**
     * sap xep day a
     * @param a
     * @param n so phan tu
     */
    public static void sort(int a[], int n){
        for (int i = 0; i < n; i++){
            for (int j = i + 1 ; j < n; j++){
                if (a[i] <= a[j]) {
                    int c = a[i];
                    a[i] = a[j];
                    a[j] = c;
                }
            }
        }
    }

    /**
     * ham main
     * @param args
     */
    public static void main(String[] args){
        int[] a = {4, 2, 1, 3, 5, 7, 8};
        sort(a, 7);
        for(int i = 0; i < 7; i++)
            System.out.println(a[i]);;
    }
}
