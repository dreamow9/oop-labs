package Bai1;

/**
 * tim uoc chung lon nhat
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class UCLN {
    /**
     * tim UCLN cua a va b
     * @param a bien dau tien
     * @param b bien thu hai
     * @return uoc chung lon nhat
     */
    public static int findGCD(int a, int b){
        if (a < b){
            int c = b;
            b = a;
            a = c;
        }
        int tmp;
        while (b != 0){
            tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    /**
     * ham main
     * @param args khong su dung
     */
    public static void main(String[] args){
        int a = 15;
        int b = 5;
        System.out.println(findGCD(a, b));
    }
}
