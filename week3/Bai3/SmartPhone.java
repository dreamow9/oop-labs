package Bai3;

/**
 * class smart phone
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class SmartPhone {
    private int price;
    private String name;
    private int screenSize;
    private int weight;

    /**
     * getter
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * setter
     * @param price price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * getter
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter
     * @return screen size
     */
    public int getScreenSize() {
        return screenSize;
    }

    /**
     * setter
     * @param screenSize screen size
     */
    public void setScreenSize(int screenSize) {
        this.screenSize = screenSize;
    }

    /**
     * getter
     * @return phone's weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * setter
     * @param weight weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }
}
