/**
* StudentManager la 1 ung dung cho phep nguoi dung khoi tao, xoa va in ra danh sach cac sinh vien
* @author Pham Duc Duy
* @version 1.0
* @since 2018-09-11
*/
/**
* class StudentManagement de quan ly sinh vien
*/
public class StudentManagement
{
  //tao mot mang cac phan thu thuoc kieu Student, mang co toi da 100 phan tu
  static public Student[] svs = new Student[100];
  /**
  * sameGroup giup kiem tra 2 sv co thuoc cung 1 lop hay ko
  * @param Student s1 la sv thu nhat can so sanh
  * @param Student s2 la sv thu hai can so sanh
  * @return true or false
  */
  public static boolean sameGroup(Student s1, Student s2){
    if (s1.getGroup() != s2.getGroup()) return false;
    return true;
  }
  /**
  * studentsByGroup giup in ra danh sach sinh vien theo lop
  * @return khong tra lai gia tri nao
  */
  public static void studentsByGroup()
  {
    //khai bao 5 phan tu sinh vien
    svs[0] = new Student("a", "69", "lop1", "e", 0);
    svs[1] = new Student("b", "69", "lop2", "e", 0);
    svs[2] = new Student("c", "69", "lop3", "e", 0);
    svs[3] = new Student("d", "69", "lop2", "e", 0);
    svs[4] = new Student("e", "69", "lop1", "e", 0);
    for (int i = 0; i < 5; i++){
      if (svs[i].getMarked() == 0){
        for (int j = i; j < 5; j++){
          if ((svs[j].getMarked() == 0) && (svs[j].getGroup().equals(svs[i].getGroup()))){
            System.out.println(svs[j].getInfo());
            svs[j].setMarked(1);
          }
        }
      }
    }
  }
  /**
	* removeStudent la ham de xoa di sinh vien co ma sv la id trong danh sach
	* @param Sring id la bien duy nhat chi id cua sv can xoa
	* @return ko co return
	*/
  public void removeStudent(String id){
    //duyet tu dau den cuoi danh sach
    for (int i = 0; i < svs.length; i++){
      //phat hien sv co mssv can tim
      if (svs[i].getId().equals(id)){
        //xoa sv ra khoi mang
        for (int j = i; j < svs.length - 1; j++){
          //thay the phan tu hien tai bang phan tu dung sau trong mang
          svs[j] = svs[j + 1];
        }
        break;
      }
    }
  }
  /**
  * main la ham thuc hien cac thao tac chinh
  * @param args ko su dung
  * @return ham main ko tra ve gia tri nao
  */
  public static void main(String[] args)
  {

    studentsByGroup();
    /**
    //tao sinh vien 1 voi gia tri mac dinh
    Student sv1 = new Student();
    //tao sinh vien 2 voi gia tri mac dinh
    Student sv2 = new Student();
    //tao sinh vien 3 voi gia tri mac dinh
    Student sv3 = new Student();
    //doi ten sinh vien 1 thanh Pham Duy
    sv1.setName("Pham Duy");
    //doi mssv cua sv1 thanh 11171963
    sv1.setId("11171963");
    //doi email cua sv1 thanh dreamow9
    sv1.setEmail("dreamow9@gmail.com");
    //doi lop cho sv1 thanh 1
    sv1.setGroup("1");
    //doi lop cho sv3 thanh k59clc
    sv3.setGroup("K59CLC");
    //in ra ten cua sv1
    System.out.println(sv1.getName());
    //in ra info cua sv1
    System.out.println(sv1.getInfo());
    //in ra info sv2
    System.out.println(sv2.getInfo());
    //in ra info cua sv3
    System.out.println(sv3.getInfo());
    //in ra ket qua kiem tra sv2 va sv3 co cung lop hay ko
    System.out.println(sameGroup(sv2, sv3));
  }*/
  }
}
/**
* class Student luu cac thuoc tinh va ham chuc nang cua lop student
*/
class Student {
  private String name;
  private String id;
  private String group;
  private String email;
  private int marked;
  /**
	* ham khoi tao sinh vien voi cac gia tri mac dinh
	*/
  public Student(){
    this.name = "Student";
    this.id = "000";
    this.group = "K59CB";
    this.email = "uet@vnu.edu.vn";
  }
  public Student(String n, String sid, String gr, String em, int m){
    this.name = n;
    this.id = sid;
    this.group = gr;
    this.email = em;
    this.marked = m;
  }
  /**
	* ham khoi tao sinh vien voi cac gia tri duoc nhap vao
	* @param Stirng name la bien dau tien. the hien ten cua sinh vien
	* @param String id la bien thu 2. the hien mssv cua sinh vien
	* @param String email la bien thu 3. the hien email cua sinh vien
	*/
  public Student(String n, String sid, String em){
    this.name = n;
    this.id = sid;
    this.group = "K59CB";
    this.email = em;
  }
  /**
	* ham khoi tao sinh vien voi cac gia tri giong voi 1 sinh vien khac
	* @param Student la bien duy nhat. chua cac gia tri cua sinh vien khac
	*/
  public Student(Student s){
    this.name = s.name;
    this.id = s.id;
    this.group = s.group;
    this.email = s.email;
  }
  /**
  * getName de lay ten cua sv
  * @return ham tra lai gia tri ten cua sv
  */
  public String getName(){
    return this.name;
  }
  /**
  * ham setName de dat ten cho sv
  * @param String n la ten cua sv
  * @return ham ko tra lai gia tri nao
  */
  public void setName(String n){
    this.name = n;
  }
  /**
  * getId de lay mssv cua sv
  * @return ham tra lai gia tri mssv cua sv
  */
  public String getId(){
    return this.id;
  }
  /**
  * ham setId de dat mssv cho sv
  * @param String n la mssv cua sv
  * @return ham ko tra lai gia tri nao
  */
  public void setId(String n){
    this.id = n;
  }
  /**
  * getGroup de lay lop cua sv
  * @return ham tra lai gia tri lop cua sv
  */
  public String getGroup(){
    return this.group;
  }
  /**
  * ham setGroup de dat lop cho sv
  * @param String n la lop cua sv
  * @return ham ko tra lai gia tri nao
  */
  public void setGroup(String n){
    this.group = n;
  }
  /**
  * getEmail de lay email cua sv
  * @return ham tra lai gia tri email cua sv
  */
  public String getEmail(){
    return this.email;
  }
  /**
  * ham setEmail de dat email cho sv
  * @param String n la email cua sv
  * @return ham ko tra lai gia tri nao
  */
  public void setEmail(String n){
    this.email = n;
  }
  /**
  * getInfo de lay thong tin cua sv
  * @return ham tra lai 1 chuoi gom ten, mssv, lop va email cua sv
  */
  public String getInfo(){
    String info = name + " " + id + " " + group + " " + email;
    return info;
  }
  /**
  * getMarked de kiem tra xem sv da duoc danh dau hay chua
  * @return tra lai trang thai cua sinh vien
  */
  public int getMarked(){
    return this.marked;
  }
  /**
  * setMakerd thay doi trang thai da duoc danh dau cua phan tu
  * @param int s la so trang thai nhap vao
  */
  public void setMarked(int s){
    this.marked = s;
  }
}
