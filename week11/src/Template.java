/**
 * @author Pham Duc Duy
 * @since 27/11/2018
 * @version 1.0
 */

import java.util.ArrayList;

public class Template {
    /**
     * sap xep cac phan tu
     * @param a day dua vao
     * @param <T> kieu du lieu
     * @return day duoc sap xep
     */
    public <T extends Comparable> T[] sort(T[] a){
        for (int i = 0; i < a.length; i++)
            for (int j = i + 1; j < a.length; j++){
                if (a[i].compareTo(a[j]) > 0)
                {
                    T tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        return a;
    }

    /**
     * tim phan tu lon nhat
     * @param a list duoc dua vao
     * @param <T> kieu du lieu
     * @return phan tu lon nhat
     */
    public <T extends Comparable> T findMax(ArrayList<T> a){
        T ans = a.get(0);
        for (int i = 1; i < a.size(); i++){
            if (a.get(i).compareTo(ans) > 0)
                ans = a.get(i);
        }
        return ans;
    }
}
