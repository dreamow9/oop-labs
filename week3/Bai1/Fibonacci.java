package Bai1;

/**
 * tinh so fibonacci
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Fibonacci {
    /**
     * tinh so fibonacci thu n
     * @param n bien duy nhat
     * @return so fibonacci
     */
    public static int fibonacci(int n){
        if (n < 0) return -1;
        if (n == 0) return 0;
        if (n == 1) return 1;
        return (fibonacci(n - 2) + fibonacci(n - 1));
    }

    /**
     * ham main
     * @param args khong su dung
     */
    public static void main(String[] args){
        System.out.println(fibonacci(9));
    }
}
