import java.io.*;
import java.util.Scanner;

public class Utils {
    /**
     * doc noi dung tu file
     * @param path ten file can doc
     */
    public static void readContentFromFile(String path)
    {
        String line = "";
        try {
            File openFile = new File(path);
            Scanner sc = new Scanner(openFile);
            while(sc.hasNextLine()){
                line = sc.nextLine();
                System.out.println(line);
            }
        } catch (FileNotFoundException e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * viet noi dung vao file
     * @param path ten file can viet
     */
    public static void writeContentToFile(String path)
    {
        try {
            FileWriter writer = new FileWriter(path);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write("this is a message\n");
            buffer.close();
        } catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * viet them noi dung vao file
     * @param path ten file can viet
     */
    public static void appendContentToFile(String path)
    {
        try
        {
            FileWriter writer = new FileWriter(path, true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write("more message");
            buffer.close();
        } catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * tim kiem file trong thu muc
     * @param folderPath duong dan thu muc
     * @param fileName ten file
     * @return file can tim
     */
    public static File findFileByName(String folderPath, String fileName) // a
    {
        File dir = new File(folderPath);
        if (dir.isFile()) return null;
        FilenameFilter filter = new FilenameFilter(){
            public boolean accept (File dir, String name)
            {
                return name.equals(fileName);
            }
        };
        String[] children = dir.list(filter);
        if (children == null)
        {
            return null;
        } else
            {
            File found = new File (fileName);
            return found;
            }
    }
    /**
     * ham main test cac chuc nang
     * @param args
     */
    public static void main(String[] args)
    {
        writeContentToFile(".\\src\\out.txt");
        appendContentToFile(".\\src\\out.txt");
        if(findFileByName("F:\\Code\\java\\week9\\src", "inp.txt") != null)
        {
            System.out.println("found file");
        }
            else System.out.println("not found");
    }
}