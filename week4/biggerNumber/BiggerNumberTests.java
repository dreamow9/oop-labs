import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


public class BiggerNumberTests {
	@Test
	public void test1() {
		BiggerNumber test = new BiggerNumber();
		int a = 15;
		int b = 5;
		int res = test.findBiggerNumber(a, b);
		assertEquals(15, res);
	}
	@Test
	public void test2() {
		BiggerNumber test = new BiggerNumber();
		int a = 15;
		int b = 30;
		int res = test.findBiggerNumber(a, b);
		assertEquals(30, res);
	}
	@Test
	public void test3(){
		BiggerNumber test = new BiggerNumber();
		int a = 15;
		int c = -5;
		int res = test.findBiggerNumber(a, c);
		assertEquals(15, res);
	}
	@Test
	public void test4(){
		BiggerNumber test = new BiggerNumber();
		int c = -5;
		int d = -150;
		int res = test.findBiggerNumber(d, c);
		assertEquals(-5, res);
	}
	@Test
	public void test5(){
		BiggerNumber test = new BiggerNumber();
		int d = -150;
		int e = 150;
		int res = test.findBiggerNumber(e, d);
		assertEquals(150, res);
	}
}
