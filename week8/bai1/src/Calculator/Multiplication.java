package Calculator;

/**
 * class nhan
 */
public class Multiplication extends BinaryExpression {
    private Expression left;
    private Expression right;
    /**
     * constructor
     * @param left
     * @param right
     */
    public Multiplication(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Expression left() {
        return left;
    }

    @Override
    public Expression right() {
        return right;
    }

    @Override
    public String toString() {
        int sum = left().evaluate() * right().evaluate();
        return sum + "";
    }

    @Override
    public int evaluate() {
        return left().evaluate() * right().evaluate();
    }
}
