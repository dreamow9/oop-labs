import org.junit.Test;
import static org.junit.Assert.*;
/**
* Smallest helps us find the smallest number in an array
*@author Pham Duc Duy
*@version 1.0
*@since 25/09/2018
*/
public class Smallest{
	/**
	* findSmallestNumber finds the smallest number
	*@param a[] the array a
	*@param n the size of a
	*@return the smallest number
	*/
	public static int findSmallestNumber(int a[], int n){
		int ans = a[0];
		for (int i = 0; i < n; i++){
			if (ans > a[i]) ans = a[i];
		}
		return ans;
	}
}