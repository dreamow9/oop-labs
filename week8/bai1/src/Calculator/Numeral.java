package Calculator;

import Calculator.Expression;

public class Numeral extends Expression {
    private int value;
    /**
     * ham khoi tao mac dinh
     */
    public Numeral() {
    }
    /**
     * ham khoi tao voi bien dau vao
     * @param v
     */
    public Numeral(int v){
        this.value = v;
    }

    @Override
    public String toString() {
        return this.value + "";
    }

    @Override
    public int evaluate() {
        return this.value;
    }
}
