package Bai3;

/**
 * class song
 * @author Pham Duc Duy
 * @since 2018
 * @version 1.0
 */
public class Song {
    private String name;
    private String author;
    private String lyrics;

    /**
     * getter
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * setter
     * @param author author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * getter
     * @return lyrics
     */
    public String getLyrics() {
        return lyrics;
    }

    /**
     * setter
     * @param lyrics lyrics
     */
    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }
}
