/**
* BMI helps us calculate the BMI index
*@author Pham Duc Duy
*@version 1.0
*@since 25/09/2018
*/
public class BMI{
	/**
	* BMICal to calcute and give conclusion
	*@param weight the fist param
	*@param height the second param
	*@return the conclusion
	*/
	public static String BMICal(double weight, double height){
		double BMIIndex = weight / (height * height);
		if (BMIIndex < 18.5) return "Thieu can";
		if (BMIIndex >= 18.5 && BMIIndex < 23) return "Binh thuong";
		if (BMIIndex >= 23 && BMIIndex < 25) return "Thua can";
		return "Beo phi";
	}
}