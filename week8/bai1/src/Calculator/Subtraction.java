package Calculator;

/**
 * class tru
 */
public class Subtraction extends BinaryExpression {
    private Expression left;
    private Expression right;
    /**
     * constructor
     * @param left
     * @param right
     */
    public Subtraction(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Expression left() {
        return left;
    }

    @Override
    public Expression right() {
        return right;
    }

    @Override
    public String toString() {
        int sum = left().evaluate() - right().evaluate();
        return sum + "";
    }

    @Override
    public int evaluate() {
        return left().evaluate() - right().evaluate();
    }
}
