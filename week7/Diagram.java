package DrawProgram;
import java.util.*;
/**
* class Diagram chua cac layer
*/
public class Diagram{
	private ArrayList<Layer> layers;
	/**
	* constructor
	*/
	public Diagram(){
		layers = new ArrayList<Layer>();
	}
	/**
	* them layer
	*@param l layer
	*/
	public void addLayer(Layer l){
		layers.add(l);
	}
	/**
	* xoa cac circle trong diagram
	*/
	public void removeCircle(){
		for (int i = 0; i < layers.size(); i++)
			layers.get(i).deleteCircle();
	}
	/**
	* xep cac loai hinh ve vao layer
	*/
	public void arrangeShapes(){
		Layer circleLayer = new Layer();
		Layer hexagonLayer = new Layer();
		Layer rectangleLayer = new Layer();
		Layer squareLayer = new Layer();
		Layer triangleLayer = new Layer();
		for (int i = 0; i < layers.size(); i++)
			for (int j = 0; j < layers.get(i).getShapes().size(); j++){
					Shape tmp = new Shape();
					tmp = layers.get(i).getShapes().get(j);
					if (tmp instanceof Circle) circleLayer.addShape(tmp);
					if (tmp instanceof Hexagon) hexagonLayer.addShape(tmp);
					if (tmp instanceof Square) squareLayer.addShape(tmp);
						else if (tmp instanceof Rectangle) rectangleLayer.addShape(tmp);
					if (tmp instanceof Triangle) triangleLayer.addShape(tmp);
			}
		layers.clear();
		if (circleLayer.getShapes().size() != 0) layers.add(circleLayer);
		if (hexagonLayer.getShapes().size() != 0) layers.add(hexagonLayer);
		if (rectangleLayer.getShapes().size() != 0) layers.add(rectangleLayer);
		if (squareLayer.getShapes().size() != 0) layers.add(squareLayer);
		if (triangleLayer.getShapes().size() != 0) layers.add(triangleLayer);
	}
	/**
	* in ra diagram sau khi da sap xep cac hinh ve
	*/
	public void printArrangedShapes(){
		for (int i = 0; i < layers.size(); i++){
			System.out.println(layers.get(i).getShapes().get(0).getClass() + ": ");
			for (int j = 0; j < layers.get(i).getShapes().size(); j++){
					layers.get(i).getShapes().get(j).getInfo();
			}
			System.out.println("-----------------------------");
		}
	}
	/**
	* in ra cac phan tu trong diagram
	*/
	public void printDiagram(){
		for (int i = 0; i < layers.size(); i++){
			layers.get(i).printLayer();
		}
	}
}
